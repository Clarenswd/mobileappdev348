﻿using LiftMeAppPrototype.Common;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Runtime.Serialization;
using System.Net.Http;
using Windows.UI.Popups;
using System.Threading.Tasks;
using Windows.UI.ApplicationSettings;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace LiftMeAppPrototype
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {

        public static MobileServiceClient mobileService = new MobileServiceClient(
            "https://inb348.azure-mobile.net/",
            "YxmGrIwWYmvAnMYTCcyMzqYgAcnveG19");

        public static MobileServiceCollection<Assessments, Assessments> items;
        public static IMobileServiceTable<Assessments> assessTable = App.MobileService.GetTable<Assessments>();

        public static String feedback = "Have you completed the Assessment today?";
        public static Assessment myTest;

        public static MobileServiceClient MobileService
        {
            get { return mobileService; }
        }

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            myTest = new Assessment();
        }

        // public method to insert new Assessment
        private static async void InsertAssessment(Assessments newTest)
        {
            await assessTable.InsertAsync(newTest);

        }
        public async static Task<bool> SaveAssessment()
        {

            Boolean check = await CheckBeforeSaving();
            bool result = true;

            var newTest = new Assessments
            {
                username = myTest.Username,
                testdatetime = myTest.TestDateTime,
                prescriptionexists = myTest.PrescriptionExists,
                prescriptiontaken = myTest.PrescriptionTaken,
                sleepscore = myTest.SleepScore,
                moodscore = myTest.MoodScore,
                nextpage = myTest.NextPage,
            };

            if (myTest.RecordId == "0")
            {

                InsertAssessment(newTest);
            }
            else
            {
                newTest.id = myTest.RecordId;
                UpdateAssessment(newTest);
            }
            return result;
        }

        private async static Task<bool> CheckBeforeSaving()
        {
            bool result = false;
            try
            {

                DateTime today = DateTime.Today;

                items = await assessTable
                   .Where(l => l.username == myTest.username)
                   .Where(l => l.testdatetime >= today)
                   .Where(l => l.nextpage == "Medications" || l.nextpage == "SleepPattern")
                   .ToCollectionAsync();
                //
                if (items.Count > 0)
                {
                    myTest.RecordId = items[0].id;
                    myTest.MoodScore = items[0].moodscore;
                    

                    // if the last saved screen was Medications
                    if (items[0].nextpage == "SleepPattern")
                    {

                        myTest.PrescriptionExists = items[0].prescriptionexists;
                        myTest.PrescriptionTaken = items[0].prescriptiontaken;
                        
                    } 

                    result = true;
                }
                else
                {
                    myTest.RecordId = "0";
                    result = false;
                }

                return result;
            }
            catch (HttpRequestException)
            {
                ShowError();
                return false;
            }
        }

        public async static Task<bool> CheckIfExists()
        {
            bool result = false;
            try
            {

                DateTime today = DateTime.Today;

                items = await assessTable
                   .Where(l => l.username == myTest.username)
                   .Where(l => l.testdatetime >= today)
                   .Where(l => l.nextpage == "Medications" || l.nextpage == "SleepPattern")
                   .ToCollectionAsync();
                //
                if (items.Count > 0)
                {
                    myTest.RecordId = items[0].id;
                    myTest.MoodScore = items[0].moodscore;
                    myTest.SleepScore = items[0].sleepscore;
                    myTest.PrescriptionExists = items[0].prescriptionexists;
                    myTest.PrescriptionTaken = items[0].prescriptiontaken;
                    myTest.NextPage = items[0].nextpage;
                    result = true;
                }
                else
                {
                    myTest.RecordId = "0";
                    result = false;
                }

                return result;
            }
            catch (HttpRequestException)
            {
                ShowError();
                return false;
            }
        }

        // public method to update existing Assessment
        private static async void UpdateAssessment(Assessments newTest)
        {
            await assessTable.UpdateAsync(newTest);
        }

        [DataContract(Name = "Assessments")]
        public class Assessments
        {
            [DataMember]
            public String id { get; set; }

            [DataMember]
            public String username { get; set; }

            [DataMember]
            public DateTime testdatetime { get; set; }

            [DataMember]
            public Boolean prescriptionexists { get; set; }

            [DataMember]
            public Boolean prescriptiontaken { get; set; }

            [DataMember]
            public int sleepscore { get; set; }

            [DataMember]
            public int moodscore { get; set; }

            [DataMember]
            public String nextpage { get; set; }
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {

#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = false;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                rootFrame.Navigate(typeof(Login), e.Arguments);
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            deferral.Complete();
        }

        private static async void ShowError()
        {
            MessageDialog dialog = new MessageDialog("Error occurred while querying the database.", "DB error");
            await dialog.ShowAsync();
        }

        /*
         *  App settings code for data export
         */

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += OnCommandsRequested;
        }

        private void OnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {

            args.Request.ApplicationCommands.Add(new SettingsCommand(
                "Data Export", "Data Export", (handler) => ShowCustomSettingFlyout()));
        }

        public void ShowCustomSettingFlyout()
        {
            CustomFlyout CustomSettingFlyout = new CustomFlyout();
            CustomSettingFlyout.Show();
        }
    }
}
