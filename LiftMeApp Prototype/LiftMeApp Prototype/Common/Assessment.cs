﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LiftMeAppPrototype.Common
{
    public class Assessment : INotifyPropertyChanged
    {
        public String id;
        public DateTime testDateTime;
        public Boolean prescriptionExists;
        public Boolean prescriptionTaken;
        public int sleepScore;
        public int moodScore;
        public String username;
        public String nextpage;

        public event PropertyChangedEventHandler PropertyChanged;

        // Constructor
        // params: username and datetime
        public Assessment(){

        }

        // Property for ID field
        public String RecordId
        {
            get { return id; }
            set
            {
                id = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("id");
            }
        }

        public DateTime TestDateTime
        {
            get { return testDateTime; }
            set
            {
                testDateTime = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("testDateTime");
            }
        }

        public String Username
        {
            get { return username; }
            set
            {
                username = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("username");
            }
        }

        // Property for PrescriptionExists field
        public Boolean PrescriptionExists
        {
            get { return prescriptionExists; }
            set
            {
                prescriptionExists = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("PrescriptionExists");
            }
        }

        // Property for PrescriptionTaken field
        public Boolean PrescriptionTaken
        {
            get { return prescriptionTaken; }
            set
            {
                prescriptionTaken = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("PrescriptionTaken");
            }
        }

        // Property for Mood Test Score
        public int MoodScore
        {
            get { return moodScore; }
            set
            {
                moodScore = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("MoodScore");
            }
        }

        // Property for Sleep Pattern Score
        public int SleepScore
        {
            get { return sleepScore; }
            set
            {
                sleepScore = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("SleepScore");
            }
        }

        // Property for Next Page
        public String NextPage
        {
            get { return nextpage; }
            set
            {
                nextpage = value;
                // Call NotifyPropertyChanged when the source property 
                // is updated.
                NotifyPropertyChanged("NextPage");
            }
        }

        // NotifyPropertyChanged will raise the PropertyChanged event, 
        // passing the source property that is being updated.
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
