﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace LiftMeAppPrototype
{
    public sealed partial class CustomFlyout : SettingsFlyout
    {
        public MobileServiceCollection<Assessments, Assessments> items;
        public IMobileServiceTable<Assessments> assessTable = App.MobileService.GetTable<Assessments>();
        public DateTime fromDate;
        public DateTime toDate;

        public CustomFlyout()
        {
            this.InitializeComponent();
        }

        private async void btnExportData_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                // Create sample file; replace if exists.

                StorageFile localfile = await Windows.Storage.DownloadsFolder
                    .CreateFileAsync("sample.csv", CreationCollisionOption.GenerateUniqueName);

                await getResults();

                if (items.Count > 0)
                {

                    string[] results = new string[items.Count + 1];

                    //insert headings
                    results[0] = "Date,Mood Score, Prescription Exists, Medication Taken, Sleep Score";

                    for (int i = 1; i < items.Count + 1; i++)
                    {
                        string line = items[i - 1].testdatetime.ToString();
                        line = line + "," + items[i - 1].moodscore.ToString();

                        if (items[i - 1].prescriptionexists)
                            line = line + ",YES";
                        else
                            line = line + ",NO";

                        if (items[i - 1].prescriptiontaken)
                            line = line + ",YES";
                        else
                            line = line + ",NO";

                        line = line + "," + items[i - 1].sleepscore.ToString();
                        results[i] = line;
                    }
                    await Windows.Storage.FileIO.WriteLinesAsync(localfile, results);


                }
                else
                {
                    await Windows.Storage.FileIO.WriteTextAsync(localfile, "No data found");
                }

            }
            catch (Exception exception)
            {

                System.Diagnostics.Debug.WriteLine(exception);
            }

            // Create the message dialog and set its content
            var messageDialog = new MessageDialog("Data has been successfully exported to the Downloads folder.");

            // Set the title for the message dialog
            messageDialog.Title = "Success!";

            // Set the command for the message dialog
            messageDialog.Commands.Add(new UICommand(
                "OK"));

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 0;

            // Show the message dialog
            await messageDialog.ShowAsync();

        }

        private async Task getResults()
        {
            try
            {
                DateTimeOffset dtFrom = this.fromDatePicker.Date;
                fromDate = dtFrom.DateTime.Date;

                DateTimeOffset dtTo = this.toDatePicker.Date;
                toDate = dtTo.DateTime;
                toDate = toDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                items = await assessTable
                   .Where(l => l.username == App.myTest.Username)
                   .Where(l => l.testdatetime >= fromDate && l.testdatetime <= toDate)
                   .ToCollectionAsync();

            }
            catch (HttpRequestException)
            {
                ShowError();
            }
        }

        [DataContract(Name = "Assessments")]
        public class Assessments
        {
            [DataMember]
            public String id { get; set; }

            [DataMember]
            public String username { get; set; }

            [DataMember]
            public DateTime testdatetime { get; set; }

            [DataMember]
            public Boolean prescriptionexists { get; set; }

            [DataMember]
            public Boolean prescriptiontaken { get; set; }

            [DataMember]
            public int sleepscore { get; set; }

            [DataMember]
            public int moodscore { get; set; }
        }

        private async void ShowError()
        {
            MessageDialog dialog = new MessageDialog("Error connecting to the Database.", "HTTP error");
            await dialog.ShowAsync();
        }
    }
}
