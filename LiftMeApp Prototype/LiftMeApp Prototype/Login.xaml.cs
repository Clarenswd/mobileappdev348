﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.WindowsAzure.MobileServices;
using System.Runtime.Serialization;
using System.Net.Http;
using Windows.UI.Popups;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using System.Linq.Expressions;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace LiftMeAppPrototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Login : Page
    {

        private MobileServiceCollection<Credentials, Credentials> items;
        public IMobileServiceTable<Credentials> loginTable = App.MobileService.GetTable<Credentials>();
        RotateTransform rotateTransform =   new RotateTransform();
        public Login()
        {
            this.InitializeComponent();
        }

        private async void ShowError()
        {
            MessageDialog dialog = new MessageDialog("Correctly configure the Mobile Service url and key in the sample.", "Configuration error");
            await dialog.ShowAsync();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            arrowShape.Opacity = 1;
            rotateTransform.Angle = 45;
            
            arrowShape.RenderTransform = rotateTransform;
            btnLogin.IsEnabled = false;
            btnRegister.IsEnabled = false;
            txtUsername.IsEnabled = false;
            txtPassword.IsEnabled = false;

            btnLogin.Content = "Processing...";

            getResults();   
        }

        private async void getResults()
        {
            try
            {
                items = await loginTable
                   .Where(l => l.username == txtUsername.Text)
                   .Where(l => l.password == txtPassword.Password).ToCollectionAsync();
                
                if (items.Count > 0)
                {
                    btnLogin.Content = "Done!";

                    App.myTest.Username = txtUsername.Text;
                    App.myTest.TestDateTime = DateTime.Now;

                    await Task.Delay(TimeSpan.FromSeconds(2));
                    this.Frame.Navigate(typeof(MainPage));
                }
                else
                {
                    btnLogin.IsEnabled = true;
                    btnRegister.IsEnabled = true;
                    txtUsername.IsEnabled = true;
                    txtPassword.IsEnabled = true;
                    btnLogin.Content = "Login";

                    MessageDialog dialog = new MessageDialog("The login information entered is either incorrect or doesn't exist. Please try again or register a new account.", "Incorrect Login");
                    await dialog.ShowAsync();
                }
            }
            catch (HttpRequestException)
            {
                ShowError();
            }
        }

        [DataContract(Name = "Credentials")]
        public class Credentials
        {
            [DataMember]
            public String id { get; set; }

            [DataMember]
            public String username { get; set; }

            [DataMember]
            public String password { get; set; }

            [DataMember]
            public DateTime lastlogin { get; set; }
        }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Register));
        }
    }
}
