﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace LiftMeAppPrototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        // initialise the local settings
        Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

        public MainPage()
        {
            this.InitializeComponent();

            // Binding the score to the text box
            tbProgress.Text = App.feedback;

            CheckForExisting();

            // check the local settings to determine whether to show the welcome section of the hub page or not
            if (localSettings.Values["welcome"] != null)
            {
                hubWelcome.Visibility = Visibility.Collapsed;
            }
        }

        private async void CheckForExisting()
        {
            bool check = await App.CheckIfExists();

            if (check)
                this.btnContinue.Visibility = Visibility.Visible;
            else
                this.btnContinue.Visibility = Visibility.Collapsed;

        }

        private void btnMood_Click(object sender, RoutedEventArgs e)
        {
            if (this.btnContinue.Visibility == Visibility.Collapsed)
                this.Frame.Navigate(typeof(MoodTest));
            else
            {
                if (App.myTest.NextPage == "Medications")
                    this.Frame.Navigate(typeof(Medications));
                else if (App.myTest.NextPage == "SleepPattern")
                    this.Frame.Navigate(typeof(SleepPattern));
            }
        }

        private void btnData_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DataExport));
        }

        private void btnChat_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Chat));
        }

        private void btnSleep_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SleepPattern));
        }

        private void btnMeds_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Medications));
        }

        private void btnChat(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(theChat));
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            this.btnContinue.Visibility = Visibility.Collapsed;

            if (App.myTest.NextPage == "Medications")
                this.Frame.Navigate(typeof(Medications));
            else if (App.myTest.NextPage == "SleepPattern")
                this.Frame.Navigate(typeof(SleepPattern));
        }


        private void moodTestTap(object sender, TappedRoutedEventArgs e)
        {
            if (this.btnContinue.Visibility == Visibility.Collapsed)
                this.Frame.Navigate(typeof(MoodTest));
            else
            {
                if (App.myTest.NextPage == "Medications")
                    this.Frame.Navigate(typeof(Medications));
                else if (App.myTest.NextPage == "SleepPattern")
                    this.Frame.Navigate(typeof(SleepPattern));
            }
        }

        private void openChat(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(theChat));
        }

    }
}
