﻿using LiftMeAppPrototype.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace LiftMeAppPrototype
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class Medications : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private Frame rootFrame;

        private MedicationChange myMeds;
        
        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public Medications()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
              rootFrame = Window.Current.Content as Frame;

            this.myMeds = new MedicationChange();
        }

        // A simple class to handle the Medication changes
        // This will be later used to save data to the DB
        private class MedicationChange 
        {
           

            private Boolean isPrescribedMedication;
            private Boolean isMedicationTaken;


            public Boolean PrescribedMedication
            {
                get { return isPrescribedMedication; }
                set
                {
                    isPrescribedMedication = value;
                }
            }

            public Boolean MedicationTaken
            {
                get { return isMedicationTaken; }
                set
                {
                    isMedicationTaken = value;
                }
            }
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            //when they click No to the first question 

            // Disable the buttons
            btnNo.IsEnabled = false;
            btnYes.IsEnabled = false;

            if (this.questionLabel.Text != "Did you take the prescribed medication today?")
            {
                myMeds.PrescribedMedication = false;
                myMeds.MedicationTaken = false;
            }
            else // they clicked No to the 2nd question
            {
                myMeds.MedicationTaken = false;
            }

            // save the data
            saveData();

            // redirect 
            this.questionLabel.Text = "Thanks, medical info has been saved.";
            App.feedback = "Medical Information has been saved.";

            await Task.Delay(TimeSpan.FromSeconds(2));
            //rootFrame.Navigate(typeof(MainPage));
            rootFrame.Navigate(typeof(SleepPattern));
            
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //when they click yes to the 1st question... 

            if (this.questionLabel.Text != "Did you take the prescribed medication today?")
            {
                this.questionLabel.Text = "Did you take the prescribed medication today?";
                myMeds.PrescribedMedication = true;
            }
            else // they clicked Yes to the 2nd question
            {
                myMeds.MedicationTaken = true;
                saveData();

                // disable the buttons
                btnNo.IsEnabled = false;
                btnYes.IsEnabled = false;

                // redirect 
                this.questionLabel.Text = "Thanks, Medical Info has been saved.";
                App.feedback = "Medical Information has been saved.";

                await Task.Delay(TimeSpan.FromSeconds(2));
                rootFrame.Navigate(typeof(SleepPattern));
            }                        

        }

        private async void saveData()
        {
            App.myTest.PrescriptionExists = myMeds.PrescribedMedication;
            App.myTest.PrescriptionTaken = myMeds.MedicationTaken;
            App.myTest.NextPage = "SleepPattern";
            bool result = await App.SaveAssessment();
        }
 
    }
}
