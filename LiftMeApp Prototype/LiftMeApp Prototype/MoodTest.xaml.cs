﻿using LiftMeAppPrototype.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace LiftMeAppPrototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MoodTest : Page
    {
        List<string> questions = new List<string>();
        List<string> labels = new List<string>();
        String initialLabelQ = "";

        SolidColorBrush myBrush = new SolidColorBrush(ColorHelper.FromArgb(255, 255, 140, 1));

        SolidColorBrush level1 = new SolidColorBrush(ColorHelper.FromArgb(255, 255, 0, 0)); //red
        SolidColorBrush level2 = new SolidColorBrush(ColorHelper.FromArgb(255, 205, 133, 63)); // peru
        SolidColorBrush level3 = new SolidColorBrush(ColorHelper.FromArgb(255, 255, 20, 147)); // deep pink
        SolidColorBrush level4 = new SolidColorBrush(ColorHelper.FromArgb(255, 139, 0, 139)); // dark magenta
        SolidColorBrush level5 = new SolidColorBrush(ColorHelper.FromArgb(255, 30, 144, 255)); // dodger blue


        int i = 0; //for showing the questions

        int scoreOneWayDataSource = 0; // to keep the score
        int nextValue = 0;
        MoodTestScore myScore;

        private NavigationHelper navigationHelper;
        private Frame rootFrame;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public MoodTest()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
            rootFrame = Window.Current.Content as Frame;

            questions.Add("How are you feeling today?");
            //creating labels
            //5 possible answers
            rHighest.Content = "Excellent";
            rWell.Content = "Good";
            rAverage.Content = "Average";
            rNotWell.Content = "Not so well";
            rLowest.Content = "Very low";

            this.question.Text = "Classify your answers";
            this.slider_element.Maximum = 5;
            this.question_list.Text = questions[i];
            initialLabelQ = this.question.Text;

            // Binding the score to the text box
            myScore = new MoodTestScore();
            myScore.Score = "0";
            tbScore.DataContext = myScore;

            this.btnNext.IsEnabled = false; // disable the button until the user makes a selection

            // initialise the local settings
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            // if the local setting welcome is null then change it to be not null
            if (localSettings.Values["welcome"] == null)
            {
                localSettings.Values["welcome"] = true;
            }
        }

        // A simple class to handle the Mood Test Scores
        private class MoodTestScore : INotifyPropertyChanged
        {
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;

            private String _myScore1;

            // Create the property that will be the source of the binding.
            public String Score
            {
                get { return _myScore1; }
                set
                {
                    _myScore1 = value;
                    // Call NotifyPropertyChanged when the source property 
                    // is updated.
                    NotifyPropertyChanged("Score");
                }
            }


            // NotifyPropertyChanged will raise the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }


        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }


        private void setSliderColour(int valcolor)
        {
            this.slider_element.Background = myBrush;

        }

        // Next button
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            this.slider_element.Value = 0;
            Frame rootFrame = Window.Current.Content as Frame;
            i++;

            // Display the score
            scoreOneWayDataSource = nextValue;
            myScore.Score = scoreOneWayDataSource.ToString();

            if (i < questions.Count)
            {

                this.question_list.Text = questions[i];


            }
            else if (i == questions.Count)
            {

                // disable the controls
                btnNext.IsEnabled = false;

                rHighest.IsEnabled = false;
                rWell.IsEnabled = false;
                rAverage.IsEnabled = false;
                rNotWell.IsEnabled = false;
                rLowest.IsEnabled = false;

                slider_element.IsEnabled = false;
                tbScore.IsEnabled = false;

                this.question_list.Foreground = myBrush;
                this.question_list.Text = "Thanks, mood test result has been saved.";

                //record data
                App.myTest.MoodScore = scoreOneWayDataSource;
                App.myTest.NextPage = "Medications";
                bool result = await App.SaveAssessment();

                // Provide feedback on the Main page
                App.feedback = "Mood Test score has been saved.";

                // redirect to Medications assessment form
                await Task.Delay(TimeSpan.FromSeconds(3));
                rootFrame.Navigate(typeof(Medications));
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.btnNext.IsEnabled = true;

            this.slider_element.Value = 5; // highest value
            this.slider_element.Foreground = level5;
            // generate score 
            nextValue = 5;
            scoreOneWayDataSource = nextValue;
            myScore.Score = scoreOneWayDataSource.ToString();
        }



        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            this.btnNext.IsEnabled = true;

            this.slider_element.Value = 4;
            this.slider_element.Foreground = level4;
            // generate score should be  - 4 out of 5
            nextValue = 4;
            // Display the score
            scoreOneWayDataSource = nextValue;
            myScore.Score = scoreOneWayDataSource.ToString();
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            this.btnNext.IsEnabled = true;

            this.slider_element.Value = 3;
            this.slider_element.Foreground = level3;
            // generate score should be  - 3 out of 5
            nextValue = 3;
            // Display the score
            scoreOneWayDataSource = nextValue;
            myScore.Score = scoreOneWayDataSource.ToString();
        }

        private void RadioButton_Checked_4(object sender, RoutedEventArgs e)
        {
            this.btnNext.IsEnabled = true;

            this.slider_element.Value = 2;
            this.slider_element.Foreground = level2;
            // generate score should be  - 2 out of 5
            nextValue = 2;
            // Display the score
            scoreOneWayDataSource = nextValue;
            myScore.Score = scoreOneWayDataSource.ToString();
        }

        private void RadioButton_Checked_5(object sender, RoutedEventArgs e)
        {
            this.btnNext.IsEnabled = true;

            this.slider_element.Value = 1;
            this.slider_element.Foreground = level1;
            // generate score should be  - 1 out of 5
            nextValue = 1;
            // Display the score
            scoreOneWayDataSource = nextValue;
            myScore.Score = scoreOneWayDataSource.ToString();
        }
        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }
        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }
        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void backHome(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }


        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {

            if (this.slider_element != null)
            {
                this.btnNext.IsEnabled = true;

                // generate score 
                nextValue = (int)e.NewValue;
                // Display the score
                scoreOneWayDataSource = nextValue;
                if (myScore != null)
                    myScore.Score = scoreOneWayDataSource.ToString();

                int caseSwitch = nextValue;
                switch (caseSwitch)
                {
                    case 1:
                        this.slider_element.Foreground = level1;
                        this.rLowest.IsChecked = true;
                        break;
                    case 2:
                        this.slider_element.Foreground = level2;
                        this.rNotWell.IsChecked = true;
                        break;
                    case 3:
                        this.slider_element.Foreground = level3;
                        this.rAverage.IsChecked = true;
                        break;
                    case 4:
                        this.slider_element.Foreground = level4;
                        this.rWell.IsChecked = true;
                        break;
                    case 5:
                        this.slider_element.Foreground = level5;
                        this.rHighest.IsChecked = true;
                        break;
                    default:
                        this.slider_element.Foreground = level3;
                        this.rAverage.IsChecked = true;
                        break;
                }
            }
        }

    }
}
