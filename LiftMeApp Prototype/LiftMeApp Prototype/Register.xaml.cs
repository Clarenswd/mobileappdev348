﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.WindowsAzure.MobileServices;
using System.Runtime.Serialization;
using System.Net.Http;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace LiftMeAppPrototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Register : Page
    {
        private MobileServiceCollection<Credentials, Credentials> items;
        public IMobileServiceTable<Credentials> loginTable = App.MobileService.GetTable<Credentials>();

        public Register()
        {
            this.InitializeComponent();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            // Back to login
            this.Frame.Navigate(typeof(Login));
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Check if all fields are filled correctly
            if (txtFirst.Text == "")
            {
                MessageDialog dialog = new MessageDialog("The first name field cannot be empty.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (txtLast.Text == "")
            {
                MessageDialog dialog = new MessageDialog("The last name field cannot be empty.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (txtUser.Text == "")
            {
                MessageDialog dialog = new MessageDialog("The username field cannot be empty.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (txtPass.Password == "")
            {
                MessageDialog dialog = new MessageDialog("The password field cannot be empty.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (txtPass.Password.Length < 4)
            {
                MessageDialog dialog = new MessageDialog("Your password must be at least 4 characters.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (txtAge.Text == "")
            {
                MessageDialog dialog = new MessageDialog("The age field cannot be empty.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(txtAge.Text, "^[0-9]*$") == false)
            {
                MessageDialog dialog = new MessageDialog("Your age must be a number.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (Convert.ToInt32(txtAge.Text) < 1 || Convert.ToInt32(txtAge.Text) > 99)
            {
                MessageDialog dialog = new MessageDialog("Your age needs to be between 1 and 99", "Invalid Information");
                await dialog.ShowAsync();
            }
            else if (btnMale.IsChecked == false && btnFemale.IsChecked == false)
            {
                MessageDialog dialog = new MessageDialog("Please select a gender before continuing.", "Invalid Information");
                await dialog.ShowAsync();
            }
            else
            {
                // If the form is all good then continue here
                // Deactivate all of the controls
                txtFirst.IsEnabled = false;
                txtLast.IsEnabled = false;
                txtUser.IsEnabled = false;
                txtPass.IsEnabled = false;
                txtAge.IsEnabled = false;
                btnFemale.IsEnabled = false;
                btnMale.IsEnabled = false;
                btnRegister.IsEnabled = false;

                // Determine gender
                string gender;

                if (btnMale.IsChecked == true)
                {
                    gender = "male";
                }
                else
                {
                    gender = "female";
                }

                // Check if the username already exists
                try
                {
                    items = await loginTable
                       .Where(l => l.username == txtUser.Text).ToCollectionAsync();

                    if (items.Count > 0)
                    {
                        // Reactivate all of the controls
                        txtFirst.IsEnabled = true;
                        txtLast.IsEnabled = true;
                        txtUser.IsEnabled = true;
                        txtPass.IsEnabled = true;
                        txtAge.IsEnabled = true;
                        btnFemale.IsEnabled = true;
                        btnMale.IsEnabled = true;
                        btnRegister.IsEnabled = true;

                        MessageDialog dialog = new MessageDialog("Unfortunately that username already exists. Please select another one.", "Username Exists");
                        await dialog.ShowAsync();
                    }
                    else
                    {
                        // Create new database object with the details
                        var insert = new Credentials
                        {
                            username = txtUser.Text,
                            password = txtPass.Password,
                            firstname = txtFirst.Text,
                            lastname = txtLast.Text,
                            age = Convert.ToInt32(txtAge.Text),
                            gender = gender
                        };

                        // Insert the database object into database
                        try
                        {
                            await loginTable.InsertAsync(insert);

                            // Let the user know it was done successfully and redirect to login page
                            MessageDialog dialog = new MessageDialog("Account created successfully. Please return to the login page.", "Success!");
                            dialog.Commands.Add(new UICommand("Back to Login", null, 0));
                            var commandChosen = await dialog.ShowAsync();

                            if (commandChosen.Label == "Back to Login")
                            {
                                this.Frame.Navigate(typeof(Login));
                            }
                        }
                        catch (MobileServiceConflictException f)
                        {
                            // Let the user know it was done successfully and redirect to login page
                            MessageDialog dialog = new MessageDialog("Something went wrong! Please try again.", "Invalid!");
                            dialog.ShowAsync();

                            // Reactivate all of the controls
                            txtFirst.IsEnabled = true;
                            txtLast.IsEnabled = true;
                            txtUser.IsEnabled = true;
                            txtPass.IsEnabled = true;
                            txtAge.IsEnabled = true;
                            btnFemale.IsEnabled = true;
                            btnMale.IsEnabled = true;
                            btnRegister.IsEnabled = true;
                        }
                    }
                }
                catch (HttpRequestException)
                {
                    ShowError();
                }
            }
        }

        private async void ShowError()
        {
            MessageDialog dialog = new MessageDialog("Correctly configure the Mobile Service url and key in the sample.", "Configuration error");
            await dialog.ShowAsync();
        }

        [DataContract(Name = "Credentials")]
        public class Credentials
        {
            [DataMember]
            public String id { get; set; }

            [DataMember]
            public String username { get; set; }

            [DataMember]
            public String password { get; set; }

            [DataMember]
            public DateTime lastlogin { get; set; }

            [DataMember]
            public String firstname { get; set; }

            [DataMember]
            public String lastname { get; set; }

            [DataMember]
            public int age { get; set; }

            [DataMember]
            public String gender { get; set; }
        }
    }
}
