﻿using LiftMeAppPrototype.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace LiftMeAppPrototype
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class SleepPattern : Page
    {

        private NavigationHelper navigationHelper;
        private Frame rootFrame;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private SleepRecord mySleep;

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public SleepPattern()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
            rootFrame = Window.Current.Content as Frame;

            this.mySleep = new SleepRecord();
            this.btnSave.IsEnabled = false;
        }

        private class SleepRecord
        {


            private int score;


            public int Score
            {
                get { return score; }
                set
                {
                    score = value;
                }
            }

        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        // Save button click event
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // save the sleep pattern score
            saveData();

            // disable the controls
            rVWell.IsEnabled = false;
            rWell.IsEnabled = false;
            rAvg.IsEnabled = false;
            rNWell.IsEnabled = false;
            rNone.IsEnabled = false;

            btnSave.IsEnabled = false;

            // redirect to Home page
            App.feedback = "Sleep Information has been saved.";

            // Thank the user for completing the assessment, let them know about data export then redirect to home page
            MessageDialog dialog = new MessageDialog("To export " + 
                "previous data select 'Data Export' from the Settings charm in the sidebar on the right " + 
                "(accessed by moving your cursor to the top-right of the screen). ", "Thank you!");
            dialog.Commands.Add(new UICommand("Okay", null, 0));
            var commandChosen = await dialog.ShowAsync();

            if (commandChosen.Label == "Okay")
            {
                this.Frame.Navigate(typeof(MainPage));
            }
        }

        /// <summary>
        /// @author Kira Jamison
        /// </summary>
        private async void saveData()
        {
            App.myTest.SleepScore = mySleep.Score;
            App.myTest.NextPage = "complete";
            bool result = await App.SaveAssessment(); // last question in the assessment
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            // generate the score
            mySleep.Score = 5;
            this.btnSave.IsEnabled = true;
        }


        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            mySleep.Score = 4;
            this.btnSave.IsEnabled = true;
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            mySleep.Score = 3;
            this.btnSave.IsEnabled = true;
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            mySleep.Score = 2;
            this.btnSave.IsEnabled = true;
        }

        private void RadioButton_Checked_4(object sender, RoutedEventArgs e)
        {
            mySleep.Score = 1;
            this.btnSave.IsEnabled = true;
        }
    }
}
