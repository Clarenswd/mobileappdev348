﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using LiftMeAppPrototype.Common;
using System.Diagnostics;

using System.IO;

using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;

using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Quobject.SocketIoClientDotNet.Client;
using Windows.UI.Popups;

 
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace LiftMeAppPrototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// 
    /// There is a web interface that will be sending data through "hey", and receive data through "hi"
    /// The app receives data on hey and send through hi.
    /// So one can customize messages and UI via two different channels ...
    /// Inspiration on the following link
    ///http://open.spotify.com/track/2mNUSPQ9NfBlSrrpd6Isor
    /// </summary>
    public sealed partial class theChat : Page
    {
        public Border docMessage;
        public Socket socket;
        SolidColorBrush borderBrushx = new SolidColorBrush(ColorHelper.FromArgb(255, 255, 0, 0));
        public CoreDispatcher dispatcher;


        public string msgIsent;


        public theChat()
        {


            this.InitializeComponent();
 
 

            dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            socket = IO.Socket("http://clarenswd.azurewebsites.net/");
            socket.On(Socket.EVENT_CONNECT, () =>
            {
               // socket.Emit("hey", "A new user connected!");


                socket.On("hey", (data) =>
                {//Add incomming Messages
                    afuncx(data.ToString(), true);
                });


            });


        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string msg = this.message.Text;
            sendMessage(msg);
            //LayoutDesign(msg, false);
            this.message.Text = "";

        }
        private void sendMessage(string msg)
        {
            try
            {
                this.msgIsent = msg;

                socket.Emit("hey", msg);

            }
            catch (Exception e)
            {
                ShowMessage(e.Message + "::Connection error, something went wrong!", "Error ");

            }

        }
        private async void afuncx(string msg, bool receiving)
        {

            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                LayoutDesign(msg, receiving);

            });
        }


        private void LayoutDesign(String message, bool receiving)
        {
            //Green Color
            SolidColorBrush BgReceive = new SolidColorBrush(ColorHelper.FromArgb(250, 5, 94, 62));

            //whiteColor
            SolidColorBrush BgSend = new SolidColorBrush(ColorHelper.FromArgb(250, 255, 255, 255));

            StackPanel DeptStackPanel = new StackPanel();
            DeptStackPanel.Margin = new Thickness(20, 5, 20, 10);

            LayoutRoot.Children.Add(DeptStackPanel);
            Grid.SetColumn(DeptStackPanel, 1);
            Grid.SetRow(DeptStackPanel, 1);

            TextBlock DeptListHeading = new TextBlock();

            DeptListHeading.Padding = new Thickness(15);

            if (message.Equals(this.msgIsent))
            {
                DeptStackPanel.Background = BgSend;
                DeptListHeading.Foreground = BgReceive;
                DeptListHeading.Text = "You said:\n\n " + message;
            }
            else
            {

                DeptStackPanel.Background = BgReceive;
                DeptListHeading.Foreground = BgSend;
                DeptListHeading.Text = "Anonymous said:\n\n " + message;
              
            }

            DeptStackPanel.Children.Add(DeptListHeading);

        }


        private async void ShowMessage(String title, String msg)
        {
            MessageDialog dialog = new MessageDialog(msg, title);
            await dialog.ShowAsync();
        }

        private void endConversation_Click(object sender, RoutedEventArgs e)
        {
            socket.Emit("hey", "The user left, thanks for the support!");
            socket.Disconnect();
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void OnKeyDownHandler(object sender, KeyRoutedEventArgs e)
        {
          
        }

    }
}
