﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Lift_Me_App_Prototype
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DataExportLogo : Page
    {
        public DataExportLogo()
        {
            this.InitializeComponent();
        }
		
		private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void btnExportData_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                // Create sample file; replace if exists.
                
                StorageFile localfile = await DownloadsFolder.CreateFileAsync("sample.csv", CreationCollisionOption.FailIfExists);
                
                // Get the current date.
                DateTime thisDay = DateTime.Today;

                // write today's date to the file
                //await Windows.Storage.FileIO.WriteTextAsync(localfile, thisDay.ToString("D"));

                // Write sample text to the file
                await Windows.Storage.FileIO.WriteTextAsync(localfile, "Exporting From: 1 Aug 2014 to 31 Aug 2014");
                
            } catch (Exception exception) {
                
                System.Diagnostics.Debug.WriteLine(exception);
            }
            
        }
    }
}
