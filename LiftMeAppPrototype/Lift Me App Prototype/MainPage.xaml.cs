﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Lift_Me_App_Prototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnVid_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Videos));
        }

        private void btnMsg_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Messages));
        }

        private void btnChat_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Chat));
        }

        private void btnHug_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Hug));
        }

        private void btnSpecial_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Specialist));
        }

        private void btnPic_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Picture));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MoodTest));
        }
    }
}
