﻿using System;
using System.Collections;
using System.Collections.Generic;
 
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Lift_Me_App_Prototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MoodTest : Page
    {
        List<string> questions = new List<string>();
        String initialLabelQ = "";
        public MoodTest()
        {
            this.InitializeComponent();
            questions.Add("How do you feel?");
            questions.Add("How big are your problems?");
            questions.Add("how much pain do you feel now?");
            questions.Add("how much stressed are you?");
            questions.Add("how much angry are you?");
            questions.Add("how much ansious are you");
            questions.Add("do you really want to cry?");
            questions.Add("how much help do you need?");

            this.question_list.Text = "";
            this.slider_element.Maximum = questions.Count();
            this.question_list.Text = questions[0];
            initialLabelQ=this.question.Text;

        }


            
     
      
        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {       
              string msg = String.Format("{0}",e.NewValue);
              this.question.Text = String.Format("{0}-{1}-From 1 - 10", this.initialLabelQ, msg);
              if ((int)e.NewValue != 0)
              {
                  this.question_list.Text = questions[((int)e.NewValue) - 1];
              }
              else {
                  this.question_list.Text = questions[0];
              }
                
               
 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
